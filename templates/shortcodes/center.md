<div style="display: block; text-align: center">
  {{ body | markdown(inline=true) | safe }}
</div>
