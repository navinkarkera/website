+++
title = "Simple Workflow Demo"
description = "Automation, Business Process Automation, Custom Softwares, Automated Solutions."
date = "2021-05-13"
template = "non-post-page.html"

[extra]
toc = true
+++

{{ youtube(id="S0fhK6X7iPY") }}


**A simple workflow demo of report generation using `Python` and `Prefect`.**

This is an example of automating a process of combining 100 comma separated files,  generating a single spreadsheet while filtering over Country and sending the generated report over email


### Client Requirement

- 100 comma separated files scattered in a directory
- Combine all reports
- Filter by given Country
- Send report via email


### Flow

{% center() %}
![Flow Diagram](workflow.png)
{% end %}

>**This is a very straight forward and simple example for basic understanding of how a process can be automated.**

>**It doesn't matter if your process is as simple as this one or complex with many unsure steps, it can be automated.**
